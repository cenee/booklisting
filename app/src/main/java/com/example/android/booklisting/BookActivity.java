package com.example.android.booklisting;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.List;

import static android.view.View.GONE;

public class BookActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<List<Book>> {
    // Tag for log messages
    public static final String LOG_TAG = BookActivity.class.getName();
    // Variable for GoogleBooks query
    private static String gBooksUrl = "";
    // Loading animation variable
    ProgressBar mSpinner;
    // TextView that is displayed when the list is empty
    TextView mEmpty;
    // ImageView that is displayed when the list is empty
    ImageView mEmptyImage;
    // Adapter for the list of books
    private BookAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.book_activity);

        // Set the url value from intent
        gBooksUrl = getIntent().getStringExtra("EXTRA_KEYWORD");
        // Find a reference to the {@link ListView} in the layout
        ListView bookListView = (ListView) findViewById(R.id.list);

        // Create a new adapter that takes an empty list of books as input
        mAdapter = new BookAdapter(this, new ArrayList<Book>());
        //Create a spinner loading animation
        mSpinner = (ProgressBar) findViewById(R.id.loading_spinner);
        //Create empty TextView
        mEmpty = (TextView) findViewById(R.id.empty_text_view);
        // Create image for empty view
        mEmptyImage = (ImageView) findViewById(R.id.empty_image_view);
        // Create Connectivity Manager to connect the internet
        ConnectivityManager mCm = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        // Get the connectivity information
        NetworkInfo activeNetwork = mCm.getActiveNetworkInfo();
        // Check the connection
        // If there is a network connection available, fetch data
        if (activeNetwork != null && activeNetwork.isConnected()) {
            // Get a reference to the LoaderManager, in order to interact with loaders.
            LoaderManager loaderManager = getSupportLoaderManager();

            // Initialize the loader. Pass in the int ID of defined loader(0) above and pass in null for
            // the bundle. Pass in this activity for the LoaderCallbacks parameter (which is valid
            // because this activity implements the LoaderCallbacks interface).
            loaderManager.initLoader(0, null, this);
            Log.v("Loader INIT", "Initialization loader is here.");
        } else {
            // Otherwise, display error
            // First, hide loading indicator so error message will be visible;
            mSpinner.setVisibility(View.GONE);
            // Update empty state with no connection error message
            mEmpty.setText(R.string.no_internet_connection);
            mEmptyImage.setImageResource(R.drawable.failure_internets);
            Log.e(LOG_TAG, "No internet connection available at the moment.");
        }
        // Set the adapter on the {@link ListView}
        // so the list can be populated in the user interface
        bookListView.setAdapter(mAdapter);

        // Set a click listener to open book store website when the list item is clicked on
        bookListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

                // Find the current book that was clicked on
                Book currentBook = mAdapter.getItem(position);
                Toast.makeText(BookActivity.this, "item " + currentBook.getmTitle() + " was clicked", Toast.LENGTH_LONG).show();
                // Get the website adress of the book
                String link_test = currentBook.getmInfoLink();
                // Convert the String URL into a URI object (to pass into the Intent constructor)
                Uri bookUri = Uri.parse(link_test);
                // Create a new intent to view the book URI
                Intent websiteIntent = new Intent(Intent.ACTION_VIEW, bookUri);
                // Check if the user's device has any app that can handle opening website
                if (websiteIntent.resolveActivity(getPackageManager()) != null) {
                    // Send the intent to launch a new activity
                    startActivity(websiteIntent);

                }
            }

        });
    }

    @Override
    public Loader<List<Book>> onCreateLoader(int id, Bundle args) {
        Log.v("CREATE", "Create method of loader is here.");
        // Create and return a new Loader that will take care of creating an Book for given url.
        return new BookLoader(this);
    }

    @Override
    public void onLoadFinished(Loader<List<Book>> loader, List<Book> books) {
        // Clear the adapter of previous earthquake data
        mAdapter.clear();
        // Hide the loading animation
        mSpinner.setVisibility(GONE);
        Log.v("FINISH", "Finish method of loader is here.");

        // If there is a valid list of {@link Book}s, then add them to the adapter's
        // data set. This will trigger the ListView to update.
        if (books != null && !books.isEmpty()) {
            mAdapter.addAll(books);
        }
        // If in any case adapter is empty
        if (mAdapter.isEmpty() || mAdapter == null) {
            // Set empty state text to display "No books found."
            mEmpty.setText(R.string.no_books_found);
            mEmptyImage.setImageResource(R.drawable.cat_out_of_fun2);
            Log.e(LOG_TAG, "No data to display because of empty state.");
        }
    }

    @Override
    public void onLoaderReset(Loader<List<Book>> loader) {
        Log.v("RESET", "Reset method of loader is here.");
        // Make sure to not longer use the adapter
        mAdapter.clear();
    }

    /*
   * Loads a list of books by using an AsyncTask to perform the
   * network request to the given URL.
   */
    private static class BookLoader extends AsyncTaskLoader<List<Book>> {
        /**
         * Constructs a new {@link BookLoader}.
         *
         * @param context of the activity
         */
        public BookLoader(Context context) {
            super(context);
        }

        @Override
        protected void onStartLoading() {
            forceLoad();
            Log.v("START-LOAD", "On start loading loader use forceload() method here.");
        }

        @Override
        public List<Book> loadInBackground() {
            Log.v("LOAD-background", "Loader is loading in background here.");
            // Make sure gBooksUrl is not null or has an empty value
            if (gBooksUrl == null) {
                return null;
            }
            // Perform the HTTP (network)request, parse the response, and extract a list of books.
            List<Book> books = QueryUtils.fetchBookData(gBooksUrl);
            Log.v("HTTP-request", "Perform network, parse response and extract data from url here.");

            // Return the books value.
            return books;
        }
    }
}