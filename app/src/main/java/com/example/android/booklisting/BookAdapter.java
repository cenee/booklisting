package com.example.android.booklisting;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.InputStream;
import java.util.ArrayList;

/**
 * Created by cini on 14.09.2017.
 */

public class BookAdapter extends ArrayAdapter<Book> {
    private Context context;

    public BookAdapter(Activity context, ArrayList<Book> books) {
        super(context, 0, books);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        //return super.getView(position, convertView, parent);

        // Check if there is an existing view list item (called convertView) that we can reuse,
        // otherwise , if convertView is null, then inflate a new new list_item layout
        View listItemView = convertView;
        if (listItemView == null) {
            listItemView = LayoutInflater.from(getContext()).inflate(
                    R.layout.list_item, parent, false);
        }

        // Find the book at the given position in thelist of books
        final Book currentbook = getItem(position);
        //Get the book author of the currentbook
        String author = currentbook.getmAuthor();
        //Get the book title of the currentbook
        String title = currentbook.getmTitle();
        //Get the infoLink of the currentbook
        String link = currentbook.getmInfoLink();
        //Get the imgLink of the currentbook
        String imgLink = currentbook.getmImage();

        // Find the TextView in the list_item.xml layout with the view ID title
        TextView titleTextView = (TextView) listItemView.findViewById(R.id.title);
        // Display the book title of the currentbook in that TextView
        titleTextView.setText(title);
        // Find the TextView in the list_item.xml layout with the view ID author
        TextView authorTextView = (TextView) listItemView.findViewById(R.id.author);
        // Display the book author of the currentbook in that TextView
        authorTextView.setText(author);
        //Find the ImageView in the list_item.xml layout with the ID book_image
        ImageView miniatureImageView = (ImageView) listItemView.findViewById(R.id.book_image);
        // Display the book image of the currentbook in that ImageView using loading in background
        if (!imgLink.isEmpty() && imgLink != null && imgLink != "") {
            new DownloadImage(miniatureImageView).execute(imgLink);
        } else {
            miniatureImageView.setImageResource(R.drawable.book);
        }

        // Return the whole list item layout (containing 2 TextViews)
        // that is showing apprpriate data in the ListView
        return listItemView;
    }

    /*
   * Downloading image by using an AsyncTask to perform the from the given URL.
   */
    class DownloadImage extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImage(ImageView bmImage) {
            this.bmImage = (ImageView) bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.d("Error", e.getStackTrace().toString());

            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }
}