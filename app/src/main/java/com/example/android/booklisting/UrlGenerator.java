package com.example.android.booklisting;

//generate query url
public class UrlGenerator {
    UrlGenerator() {
    }

    //generate string contains url GoogleBooksAPI query for JSON response
    //@param keyword is value of user text input from EditText field
    public static String getUrlByKeyword(String keyword) {
        return "https://www.googleapis.com/books/v1/volumes?q=" + keyword + "&maxResults=10";
    }
}