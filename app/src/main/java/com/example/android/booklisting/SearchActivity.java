package com.example.android.booklisting;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SearchActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_activity);

        //get search button to find books
        Button findButton = (Button) findViewById(R.id.szukaj);
        //create on button click listener to perform finding books
        findButton.setOnClickListener(new View.OnClickListener() {
            /** Called when the user taps the search button */
            public void onClick(View v) {
                //get text from editable text field
                EditText inputText = (EditText) findViewById(R.id.keyword);
                //get text from text field and removing white spaces with .trim() method
                String bookSearchKeyword = inputText.getText().toString().trim();

                if (!bookSearchKeyword.isEmpty() && bookSearchKeyword != null) {
                    String url = UrlGenerator.getUrlByKeyword(bookSearchKeyword);
                    //Create intent to other book activity screen
                    Intent intent = new Intent(SearchActivity.this, BookActivity.class);
                    //pass the url keyword
                    intent.putExtra("EXTRA_KEYWORD", url);
                    startActivity(intent);
                } else {
                    Toast.makeText(SearchActivity.this, "What book you are looking for?", Toast.LENGTH_LONG).show();
                }
            }
        });
    }
}
