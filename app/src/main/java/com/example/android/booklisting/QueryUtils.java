package com.example.android.booklisting;

import android.text.TextUtils;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import static com.example.android.booklisting.BookActivity.LOG_TAG;

public final class QueryUtils {
    private QueryUtils() {

    }

    private static List<Book> extractFeatureFromJson(String bookJSON) {

        //If theJSON string is empty or null, then return early.
        if (TextUtils.isEmpty(bookJSON)) {
            return null;
        }
        //Create an empty ArrayList that we can start adding books to
        List<Book> books = new ArrayList<>();

        try {
            // Create a JSONObject from the JSON response string
            JSONObject root = new JSONObject(bookJSON);
            //Check if JSONObject has the root key
            if (root.has("items")) {
                // Extract the JSONArray associated with the key called "items",
                // which represents a list of items (or books).
                JSONArray bookArray = root.getJSONArray("items");
                //Loop through each book in the bookArray, create an {@link Book} object
                for (int i = 0; i < bookArray.length(); i++) {
                    //Get single book position from the list of books
                    JSONObject currentbook = bookArray.getJSONObject(i);
                    //Get volume info JSONObject containing specific information about current book
                    JSONObject volumeinfo = currentbook.getJSONObject("volumeInfo");
                    //Extract the value for the key called "title" to get title of the current book
                    String title = volumeinfo.getString("title");
                    //Extract the value for the key called "infoLink" to get link of the store book page
                    String link = volumeinfo.getString("infoLink");
                    String imgLink = "";
                    if (volumeinfo.has("imageLinks")) {
                        //Get imageLinks JSON object containing info with book images links
                        JSONObject imgMiniature = volumeinfo.getJSONObject("imageLinks");

                        if (imgMiniature.has("smallThumbnail")) {
                            //Extract the miniature image of the current book
                            imgLink = imgMiniature.getString("smallThumbnail");
                        }
                    }
                    // Extract the JSONArray associated with the key called "authors"
                    String authors = "";
                    if (volumeinfo.has("authors")) {
                        JSONArray authorsArray = volumeinfo.getJSONArray("authors");
                        authors = authorsArray.getString(0);
                    } else {
                        authors = "Author N/A";
                    }

                    // Create a new {@link Book} object with title and author and book url from the JSON response.
                    Book book = new Book(title, authors, link, imgLink);
                    // Add the new {@link Book} to the list of books.
                    books.add(book);
                }
            }
        } catch (JSONException e) {
            // If an error is thrown when executing any of the above statements in the "try" block,
            // catch the exception here, so the app doesn't crash. Print a log message
            // with the message from the exception.
            Log.e("QueryUtils", "Problem parsing the book JSON results", e);
        }
        // Return list of books
        return books;
    }

    /**
     * Returns new URL object from the given string URL.
     */
    private static URL createUrl(String stringUrl) {
        URL url = null;
        try {
            url = new URL(stringUrl);
        } catch (MalformedURLException e) {
            Log.e(LOG_TAG, "Error with creating URL ", e);
        }
        return url;
    }

    /**
     * Query the GoogleBook API dataset and return an {@link Book} object to represent a single book.
     */
    public static List<Book> fetchBookData(String requestUrl) {
        //test for slow internet connection:
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
        // Create URL object
        URL url = createUrl(requestUrl);

        // Perform HTTP request to the URL and receive a JSON response back
        String jsonResponse = null;
        try {
            jsonResponse = makeHttpRequest(url);
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error closing input stream", e);
        }
        // Extract relevant fields from the JSON response and create a list of {@link Book}s
        List<Book> books = extractFeatureFromJson(jsonResponse);

        // Return the list of {@link Book}s
        return books;
    }

    /**
     * Make an HTTP request to the given URL and return a String as the response.
     */
    private static String makeHttpRequest(URL url) throws IOException {
        String jsonResponse = "";

        // If the URL is null, then return early.
        if (url == null) {
            Log.v(LOG_TAG, "JSON response is null");
            return jsonResponse;
        }
        HttpURLConnection urlConnection = null;
        InputStream inputStream = null;
        try {
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setReadTimeout(10000 /* milliseconds */);
            urlConnection.setConnectTimeout(15000 /* milliseconds */);
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();

            // If the request was successful (response code 200),
            // then read the input stream and parse the response.
            if (urlConnection.getResponseCode() == 200) {
                inputStream = urlConnection.getInputStream();
                jsonResponse = readFromStream(inputStream);
            } else {
                Log.e(LOG_TAG, "Error response code: " + urlConnection.getResponseCode());
            }
        } catch (IOException e) {
            Log.e(LOG_TAG, "Problem retrieving the book JSON results.", e);
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            if (inputStream != null) {
                inputStream.close();
            }
        }
        return jsonResponse;
    }

    /**
     * Convert the {@link InputStream} into a String which contains the
     * whole JSON response from the server.
     */
    private static String readFromStream(InputStream inputStream) throws IOException {
        StringBuilder output = new StringBuilder();
        if (inputStream != null) {
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream, Charset.forName("UTF-8"));
            BufferedReader reader = new BufferedReader(inputStreamReader);
            String line = reader.readLine();
            while (line != null) {
                output.append(line);
                line = reader.readLine();
            }
        }
        return output.toString();
    }
}