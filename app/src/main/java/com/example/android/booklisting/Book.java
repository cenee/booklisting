package com.example.android.booklisting;

//object contains information related to a single book
public class Book {
    // Title name of the book object
    private String mTitle;
    // Author name of the book object
    private String mAuthor;
    // Link to the store of the book object
    private String mInfoLink;
    // Source of image miniature
    private String mImage;

    //Book class constructors new Book object creation.
    /*
    * @param title is the title of the book
    * @param author is the author name of the book
    * @param link is the direct route to the store page of the book
    * @param img is containing miniature of the book image url address
    */
    public Book(String title, String author, String link, String img) {
        mTitle = title;
        mAuthor = author;
        mInfoLink = link;
        mImage = img;

    }

    //Returns title of the book
    public String getmTitle() {
        return mTitle;
    }

    //Returns author name of the book
    public String getmAuthor() {
        return mAuthor;
    }

    //Returns link to the store with the book
    public String getmInfoLink() {
        return mInfoLink;
    }

    //Returns url to miniature book image
    public String getmImage() {
        return mImage;
    }
}